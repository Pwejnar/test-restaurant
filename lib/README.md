Zrealizuj aplikację dla mobilnego systemu operacyjnego Android wykorzystując Fluttera obsługującą zamówienia w restauracji.

Wymagania:
min API level: 26
max API level 33

Dostępne pozycje w menu
Pizza - jeden rozmiar 32 cm
Margherita – 25 zł
Vegetariana – 27 zł
Tosca – 30 zł
Venezia – 31 zł
Dodatki do pizzy – 2.50 zł
Podwójny ser
Salami
Szynka
Pieczarki
Kurczak
Modyfikacja ciasta:
Bezglutenowe - 4 zł
Serowe brzegi - 6 zł

Dania główne
Schabowy z (do wyboru jedno z) frytkami/ryżem/ziemniakami – 35 zł
Ryba z frytkami – 33 zł
Placek po węgiersku – 25 zł
Dodatki
Bar sałatkowy – 7 zł
Zestaw sosów – 5.50 zł

Zupy:
Pomidorowa – 13.50 zł
Rosół – 12 zł
Napoje – 7 zł
Kawa
Herbata
Cola

Wykaz funkcjonalności, które powinna dostarczyć aplikacja:
Wybór dowolnego zestawu dań
Każde danie można wybrać n razy
Całkowita cena zamówienia powinna aktualizować się w czasie rzeczywistym
Możliwość dodania “Uwag do zamówienia”
Historia zamówień

Projekt widoków dowolny ale spełniający wyżej wymienione funkcjonalności.