import 'package:collection/collection.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_type_model.dart';

part 'menu_item_model.g.dart';

@JsonSerializable()
class MenuItemModel {
  final String? id;
  final String? name;
  final double? price;
  final List<MenuOptionTypeModel>? optionTypes;

  MenuItemModel({
    this.id,
    this.name,
    this.price,
    this.optionTypes,
  });

  MenuOptionTypeModel? getOptionType(String idOptionType) {
    if (optionTypes == null) return null;
    return optionTypes!.firstWhereOrNull((e) => e.id == idOptionType);
  }

  factory MenuItemModel.fromJson(Map<String, dynamic> json) {
    return _$MenuItemModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$MenuItemModelToJson(this);
  }
}
