import 'package:json_annotation/json_annotation.dart';

part 'menu_option_model.g.dart';

@JsonSerializable()
class MenuOptionModel {
  final String? id;
  final String? name;
  final double? price;

  MenuOptionModel({
    this.id,
    this.name,
    this.price,
  });

  factory MenuOptionModel.fromJson(Map<String, dynamic> json) {
    return _$MenuOptionModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$MenuOptionModelToJson(this);
  }
}
