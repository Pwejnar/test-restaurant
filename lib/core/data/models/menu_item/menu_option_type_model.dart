import 'package:json_annotation/json_annotation.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_model.dart';

part 'menu_option_type_model.g.dart';

@JsonSerializable()
class MenuOptionTypeModel {
  final String? id;
  final String? name;
  final bool? allowCheckMany;
  final List<MenuOptionModel>? options;

  MenuOptionTypeModel({
    this.id,
    this.name,
    this.allowCheckMany,
    this.options,
  });

  factory MenuOptionTypeModel.fromJson(Map<String, dynamic> json) {
    return _$MenuOptionTypeModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$MenuOptionTypeModelToJson(this);
  }
}
