// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_option_type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MenuOptionTypeModel _$MenuOptionTypeModelFromJson(Map<String, dynamic> json) =>
    MenuOptionTypeModel(
      id: json['id'] as String?,
      name: json['name'] as String?,
      allowCheckMany: json['allowCheckMany'] as bool?,
      options: (json['options'] as List<dynamic>?)
          ?.map((e) => MenuOptionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MenuOptionTypeModelToJson(
        MenuOptionTypeModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'allowCheckMany': instance.allowCheckMany,
      'options': instance.options,
    };
