import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';

part 'option_selection_model.g.dart';

@JsonSerializable()
class OptionSelectionModel {
  final String idOptionType;
  final ObservableList<String> idSelections;

  OptionSelectionModel({
    required this.idOptionType,
    required this.idSelections,
  });

  factory OptionSelectionModel.fromJson(Map<String, dynamic> json) {
    return _$OptionSelectionModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$OptionSelectionModelToJson(this);
  }
}
