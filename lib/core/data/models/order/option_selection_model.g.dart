// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'option_selection_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OptionSelectionModel _$OptionSelectionModelFromJson(
        Map<String, dynamic> json) =>
    OptionSelectionModel(
      idOptionType: json['idOptionType'] as String,
      idSelections: ObservableList<String>.of(
          (json['idSelections'] as List).map((e) => e as String)),
    );

Map<String, dynamic> _$OptionSelectionModelToJson(
        OptionSelectionModel instance) =>
    <String, dynamic>{
      'idOptionType': instance.idOptionType,
      'idSelections': instance.idSelections,
    };
