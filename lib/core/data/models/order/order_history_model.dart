import 'package:json_annotation/json_annotation.dart';
import 'package:test_restaurant/core/data/models/order/order_model.dart';

part 'order_history_model.g.dart';

@JsonSerializable()
class OrderHistoryModel {
  final List<OrderModel>? orders;

  OrderHistoryModel({
    this.orders,
  });

  factory OrderHistoryModel.fromJson(Map<String, dynamic> json) {
    return _$OrderHistoryModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$OrderHistoryModelToJson(this);
  }
}
