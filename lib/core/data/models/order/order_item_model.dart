import 'package:collection/collection.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:mobx/mobx.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/core/data/models/order/option_selection_model.dart';

part 'order_item_model.g.dart';

@JsonSerializable()
class OrderItemModel {
  final String? id;
  final MenuItemModel? menuItem;
  final ObservableList<OptionSelectionModel> selectedOptions;

  OrderItemModel({
    this.id,
    this.menuItem,
    required this.selectedOptions,
  });

  OptionSelectionModel? getOption(String idOptionType) {
    return selectedOptions.firstWhereOrNull(
      (so) => so.idOptionType == idOptionType,
    );
  }

  double getFullPrice() {
    double total = menuItem?.price ?? 0;

    for (var so in selectedOptions) {
      String idType = so.idOptionType;

      var optionTypes = menuItem?.optionTypes ?? [];
      var op = optionTypes.firstWhereOrNull((ot) => ot.id == idType);

      if (op != null) {
        var selections = so.idSelections;
        var seletedOptions = op.options!.where(
          (opt) => selections.contains(opt.id),
        );
        List<double> prices = seletedOptions.map((e) => e.price ?? 0).toList();
        var sum = prices.fold<double>(0, (a, b) => a + b);
        total += sum;
      }
    }

    return total;
  }

  factory OrderItemModel.fromJson(Map<String, dynamic> json) {
    return _$OrderItemModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$OrderItemModelToJson(this);
  }
}
