// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderItemModel _$OrderItemModelFromJson(Map<String, dynamic> json) =>
    OrderItemModel(
      id: json['id'] as String?,
      menuItem: json['menuItem'] == null
          ? null
          : MenuItemModel.fromJson(json['menuItem'] as Map<String, dynamic>),
      selectedOptions: ObservableList<OptionSelectionModel>.of(
          (json['selectedOptions'] as List).map(
              (e) => OptionSelectionModel.fromJson(e as Map<String, dynamic>))),
    );

Map<String, dynamic> _$OrderItemModelToJson(OrderItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'menuItem': instance.menuItem,
      'selectedOptions': instance.selectedOptions,
    };
