import 'package:json_annotation/json_annotation.dart';
import 'package:test_restaurant/core/data/models/order/order_item_model.dart';

part 'order_model.g.dart';

@JsonSerializable()
class OrderModel {
  final String? id;
  final List<OrderItemModel>? items;
  final String? comments;

  OrderModel({
    this.id,
    this.items,
    this.comments,
  });

  double gotTotalPrice() {
    double total = 0;

    for (var i in items ?? []) {
      total += i.getFullPrice();
    }

    return total;
  }

  factory OrderModel.fromJson(Map<String, dynamic> json) {
    return _$OrderModelFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$OrderModelToJson(this);
  }
}
