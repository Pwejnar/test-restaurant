import 'package:mobx/mobx.dart';

part 'app_state.g.dart';

class AppState = AppStateBase with _$AppState;

abstract class AppStateBase with Store {
  @observable
  bool isLoading = false;
  String? restaurantEmail;

  @action
  void setLoading(bool loading) {
    isLoading = loading;
  }

  void setRestaurantEmail(String email) {
    restaurantEmail = email;
  }
}
