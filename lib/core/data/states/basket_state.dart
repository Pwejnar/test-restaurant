import 'package:collection/collection.dart';
import 'package:mobx/mobx.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/core/data/models/order/order_item_model.dart';
import 'package:test_restaurant/core/data/models/order/option_selection_model.dart';
import 'package:uuid/uuid.dart';

part 'basket_state.g.dart';

class BasketState = BasketStateBase with _$BasketState;

abstract class BasketStateBase with Store {
  @observable
  var orderItems = ObservableList<OrderItemModel>();

  OrderItemModel getOrderItem(String idOrderItem) {
    return orderItems.firstWhere((order) => order.id == idOrderItem);
  }

  @action
  void addItemToBasket(MenuItemModel item) {
    List<OptionSelectionModel> initSelections = [];

    bool hasObligatoryItemOptionToSelect = item.optionTypes != null &&
        item.optionTypes!.any((o) => o.allowCheckMany != true);

    if (hasObligatoryItemOptionToSelect) {
      initSelections = _getInitSelection(item);
    }

    var orderItem = OrderItemModel(
      id: Uuid().v4(),
      menuItem: item,
      selectedOptions: ObservableList.of(initSelections),
    );

    orderItems.add(orderItem);
  }

  @action
  void removeItemFromBasket(String idMenuItem) {
    var item = orderItems.firstWhere(
      (orderItems) => orderItems.menuItem?.id == idMenuItem,
    );
    orderItems.remove(item);
  }

  @action
  void clearAll() {
    orderItems.clear();
  }

  bool isSelected(String idOrderItem, String idOptionType, String idOption) {
    var orderItem = orderItems.firstWhere((oi) => oi.id == idOrderItem);
    var opt = orderItem.getOption(idOptionType);
    if (opt != null) {
      return opt.idSelections.contains(idOption);
    }
    return false;
  }

  @action
  void changeSelectionState(
    String idOrderItem,
    String idOptionType,
    String idOption,
  ) {
    var orderItem = orderItems.firstWhere((oi) => oi.id == idOrderItem);
    var option = orderItem.getOption(idOptionType);

    if (option == null) {
      var os = OptionSelectionModel(
        idOptionType: idOptionType,
        idSelections: ObservableList.of([idOption]),
      );

      orderItem.selectedOptions.add(os);
    } else {
      var menuItem = orderItem.menuItem;
      var ot = menuItem!.getOptionType(idOptionType);
      bool isMultiSelect = ot?.allowCheckMany == true;

      if (isMultiSelect) {
        var o = option.idSelections.firstWhereOrNull(
          (e) => e == idOption,
        );

        bool isSelected = o != null;

        if (isSelected) {
          option.idSelections.remove(idOption);
        } else {
          option.idSelections.add(idOption);
        }
      } else {
        option.idSelections.clear();
        option.idSelections.add(idOption);
      }
    }
  }

  double getTotalCost() {
    double total = 0;

    for (var order in orderItems) {
      var price = order.getFullPrice();
      total += price;
    }

    return total;
  }

  int getBasketItemsCount(String idItem) {
    var items = orderItems.where((d) => d.menuItem?.id == idItem);
    return items.length;
  }

  List<OptionSelectionModel> _getInitSelection(MenuItemModel item) {
    List<OptionSelectionModel> initSelections = [];

    for (var o in item.optionTypes!) {
      bool isObligatory = o.allowCheckMany != true;
      bool hasOptions = o.options?.isNotEmpty == true;

      if (isObligatory == true && hasOptions) {
        var idFirst = o.options!.first.id!;

        initSelections.add(
          OptionSelectionModel(
            idOptionType: o.id!,
            idSelections: ObservableList.of([idFirst]),
          ),
        );
      }
    }

    return initSelections;
  }
}
