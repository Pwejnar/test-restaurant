// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'basket_state.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$BasketState on BasketStateBase, Store {
  late final _$orderItemsAtom =
      Atom(name: 'BasketStateBase.orderItems', context: context);

  @override
  ObservableList<OrderItemModel> get orderItems {
    _$orderItemsAtom.reportRead();
    return super.orderItems;
  }

  @override
  set orderItems(ObservableList<OrderItemModel> value) {
    _$orderItemsAtom.reportWrite(value, super.orderItems, () {
      super.orderItems = value;
    });
  }

  late final _$BasketStateBaseActionController =
      ActionController(name: 'BasketStateBase', context: context);

  @override
  void addItemToBasket(MenuItemModel item) {
    final _$actionInfo = _$BasketStateBaseActionController.startAction(
        name: 'BasketStateBase.addItemToBasket');
    try {
      return super.addItemToBasket(item);
    } finally {
      _$BasketStateBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeItemFromBasket(String idMenuItem) {
    final _$actionInfo = _$BasketStateBaseActionController.startAction(
        name: 'BasketStateBase.removeItemFromBasket');
    try {
      return super.removeItemFromBasket(idMenuItem);
    } finally {
      _$BasketStateBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearAll() {
    final _$actionInfo = _$BasketStateBaseActionController.startAction(
        name: 'BasketStateBase.clearAll');
    try {
      return super.clearAll();
    } finally {
      _$BasketStateBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeSelectionState(
      String idOrderItem, String idOptionType, String idOption) {
    final _$actionInfo = _$BasketStateBaseActionController.startAction(
        name: 'BasketStateBase.changeSelectionState');
    try {
      return super.changeSelectionState(idOrderItem, idOptionType, idOption);
    } finally {
      _$BasketStateBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
orderItems: ${orderItems}
    ''';
  }
}
