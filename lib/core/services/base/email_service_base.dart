abstract class EmailServiceBase {
  Future<bool> sendEmail(String text);
}
