import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';

abstract class MenuServiceBase {
  Future<List<MenuItemModel>> fetchPizzas();
  Future<List<MenuItemModel>> fetchMainDishes();
  Future<List<MenuItemModel>> fetchSoups();
  Future<List<MenuItemModel>> fetchDrinks();
}
