import 'package:test_restaurant/core/data/models/order/order_model.dart';
import 'package:test_restaurant/core/data/models/order/order_history_model.dart';

abstract class OrderServiceBase {
  Future<bool> createOrder(OrderModel order);
  Future<OrderHistoryModel?> fetchSavedOrders();
}
