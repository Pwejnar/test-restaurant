abstract class SettingsServiceBase {
  Future<String?> getRestaruantEmail();
  Future<bool> setRestaurantEmail(String email);
}
