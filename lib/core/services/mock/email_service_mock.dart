import 'package:test_restaurant/core/services/base/email_service_base.dart';

class EmailServiceMock implements EmailServiceBase {
  @override
  Future<bool> sendEmail(String text) async {
    //TO DO WHEN BACKEND READY
    return true;
  }
}
