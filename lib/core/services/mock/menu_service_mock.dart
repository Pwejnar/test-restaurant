import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_model.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_type_model.dart';
import 'package:test_restaurant/core/services/base/manu_service_base.dart';

class MenuServiceMock implements MenuServiceBase {
  @override
  Future<List<MenuItemModel>> fetchDrinks() async {
    return [
      MenuItemModel(id: "1", name: "Kawa", price: 7),
      MenuItemModel(id: "2", name: "Herbata", price: 7),
      MenuItemModel(id: "3", name: "Cola", price: 7),
    ];
  }

  @override
  Future<List<MenuItemModel>> fetchMainDishes() async {
    var options = _fetchOptions();
    return [
      MenuItemModel(id: "4", name: "Schabowy", price: 35, optionTypes: options),
      MenuItemModel(id: "5", name: "Ryba z frytkami ", price: 33),
      MenuItemModel(id: "6", name: "Placek po węgiersku", price: 25),
    ];
  }

  @override
  Future<List<MenuItemModel>> fetchPizzas() async {
    var options = _fetchPizzaOptions();
    return [
      MenuItemModel(
          id: "7", name: "Margherita", price: 25, optionTypes: options),
      MenuItemModel(
          id: "8", name: "Vegetariana ", price: 27, optionTypes: options),
      MenuItemModel(id: "9", name: "Tosca", price: 30, optionTypes: options),
      MenuItemModel(id: "10", name: "Venezia", price: 31, optionTypes: options),
    ];
  }

  @override
  Future<List<MenuItemModel>> fetchSoups() async {
    return [
      MenuItemModel(id: "11", name: "Pomidorowa", price: 13.5),
      MenuItemModel(id: "12", name: "Rosół ", price: 12),
    ];
  }

  List<MenuOptionTypeModel> _fetchOptions() {
    return [
      MenuOptionTypeModel(
        id: "1",
        allowCheckMany: false,
        name: "Węglowodany",
        options: [
          MenuOptionModel(id: "1", name: "Frytki", price: 0),
          MenuOptionModel(id: "2", name: "Ryż", price: 0),
          MenuOptionModel(id: "3", name: "Ziemniaki", price: 0),
        ],
      ),
    ];
  }

  List<MenuOptionTypeModel> _fetchPizzaOptions() {
    return [
      MenuOptionTypeModel(
        id: "2",
        allowCheckMany: false,
        name: "Rozmiar ciasta",
        options: [
          MenuOptionModel(id: "4", name: "32cm", price: 0),
        ],
      ),
      MenuOptionTypeModel(
        id: "3",
        allowCheckMany: true,
        name: "Dodatki",
        options: [
          MenuOptionModel(id: "5", name: "Podwójny ser", price: 2.5),
          MenuOptionModel(id: "6", name: "Salami", price: 2.5),
          MenuOptionModel(id: "7", name: "Szynka", price: 2.5),
          MenuOptionModel(id: "8", name: "Pieczarki", price: 2.5),
          MenuOptionModel(id: "9", name: "Kurczak", price: 2.5),
        ],
      ),
      MenuOptionTypeModel(
        id: "4",
        allowCheckMany: true,
        name: "Rodzaj ciasta",
        options: [
          MenuOptionModel(id: "10", name: "Bezglutenowe ", price: 4),
          MenuOptionModel(id: "11", name: "Serowe brzegi", price: 6),
        ],
      ),
    ];
  }
}
