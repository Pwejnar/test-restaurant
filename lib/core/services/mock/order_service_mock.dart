import 'dart:convert';

import 'package:test_restaurant/core/data/models/order/order_model.dart';
import 'package:test_restaurant/core/data/models/order/order_history_model.dart';
import 'package:test_restaurant/core/services/base/email_service_base.dart';
import 'package:test_restaurant/core/services/base/order_service_base.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_methods.dart';
import 'package:test_restaurant/ui/shared/shared_variables.dart';

class OrderServiceMock implements OrderServiceBase {
  var _emailService = serviceLocator.get<EmailServiceBase>();

  @override
  Future<bool> createOrder(OrderModel order) async {
    await Future.delayed(Duration(seconds: 1));
    await _saveOrderLocally(order);
    await _emailService.sendEmail(SharedMethods.getOrderDetails(order));
    return true;
  }

  Future<OrderHistoryModel?> fetchSavedOrders() async {
    final sp = await SharedPreferences.getInstance();
    var savedOrdersJson = sp.getString(SharedVariables.ordersSPKey);
    OrderHistoryModel? orderHistory;

    if (savedOrdersJson != null) {
      try {
        var map = json.decode(savedOrdersJson);
        orderHistory = OrderHistoryModel.fromJson(map);
      } catch (e) {
        print(e);
      }
    }

    return orderHistory;
  }

  Future<void> _saveOrderLocally(OrderModel order) async {
    final sp = await SharedPreferences.getInstance();

    OrderHistoryModel? orderHistory = await fetchSavedOrders();

    if (orderHistory?.orders?.isNotEmpty != true) {
      orderHistory = OrderHistoryModel(orders: [order]);
    } else {
      orderHistory!.orders!.add(order);
    }

    var js = orderHistory.toJson();
    var jsString = json.encode(js);
    sp.setString(SharedVariables.ordersSPKey, jsString);
  }
}
