import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_restaurant/core/services/base/settings_service_Base.dart';
import 'package:test_restaurant/ui/shared/shared_variables.dart';

class SettingsServiceMock implements SettingsServiceBase {
  @override
  Future<String?> getRestaruantEmail() async {
    final sp = await SharedPreferences.getInstance();
    return sp.getString(SharedVariables.emailSPKey);
  }

  @override
  Future<bool> setRestaurantEmail(String email) async {
    final sp = await SharedPreferences.getInstance();
    try {
      sp.setString(SharedVariables.emailSPKey, email);
    } catch (e) {
      print(e);
      return false;
    }
    return true;
  }
}
