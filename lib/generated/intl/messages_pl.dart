// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pl locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pl';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "coctails": MessageLookupByLibrary.simpleMessage("Napoje"),
        "mainDishes": MessageLookupByLibrary.simpleMessage("Dania główne"),
        "next": MessageLookupByLibrary.simpleMessage("Dalej"),
        "noData": MessageLookupByLibrary.simpleMessage("Brak danych"),
        "noDishes": MessageLookupByLibrary.simpleMessage("Brak pozycji"),
        "noOrders": MessageLookupByLibrary.simpleMessage("Brak zamówień"),
        "onlyLettersAllowed":
            MessageLookupByLibrary.simpleMessage("Tylko litery"),
        "optionally": MessageLookupByLibrary.simpleMessage("Opcjonalnie"),
        "orderComments":
            MessageLookupByLibrary.simpleMessage("Komentarz do zamówinia"),
        "ordersHistory":
            MessageLookupByLibrary.simpleMessage("Historia zamówień"),
        "pizzas": MessageLookupByLibrary.simpleMessage("Pizze"),
        "problemOccured":
            MessageLookupByLibrary.simpleMessage("Wystąpił priblem"),
        "restaurantEmail":
            MessageLookupByLibrary.simpleMessage("Email restauracji"),
        "saveEmail": MessageLookupByLibrary.simpleMessage("Zapisz email"),
        "saveOrder": MessageLookupByLibrary.simpleMessage("Zapisz zamówienie"),
        "soups": MessageLookupByLibrary.simpleMessage("Zupy"),
        "textTooShort":
            MessageLookupByLibrary.simpleMessage("Wartość za krótka"),
        "toChoose": MessageLookupByLibrary.simpleMessage("Do wyboru")
      };
}
