// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class L {
  L();

  static L? _current;

  static L get current {
    assert(_current != null,
        'No instance of L was loaded. Try to initialize the L delegate before accessing L.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<L> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = L();
      L._current = instance;

      return instance;
    });
  }

  static L of(BuildContext context) {
    final instance = L.maybeOf(context);
    assert(instance != null,
        'No instance of L present in the widget tree. Did you add L.delegate in localizationsDelegates?');
    return instance!;
  }

  static L? maybeOf(BuildContext context) {
    return Localizations.of<L>(context, L);
  }

  /// `Only letters allowed`
  String get onlyLettersAllowed {
    return Intl.message(
      'Only letters allowed',
      name: 'onlyLettersAllowed',
      desc: '',
      args: [],
    );
  }

  /// `Text too short`
  String get textTooShort {
    return Intl.message(
      'Text too short',
      name: 'textTooShort',
      desc: '',
      args: [],
    );
  }

  /// `Problem occured`
  String get problemOccured {
    return Intl.message(
      'Problem occured',
      name: 'problemOccured',
      desc: '',
      args: [],
    );
  }

  /// `No data`
  String get noData {
    return Intl.message(
      'No data',
      name: 'noData',
      desc: '',
      args: [],
    );
  }

  /// `Main dishes`
  String get mainDishes {
    return Intl.message(
      'Main dishes',
      name: 'mainDishes',
      desc: '',
      args: [],
    );
  }

  /// `Pizzas`
  String get pizzas {
    return Intl.message(
      'Pizzas',
      name: 'pizzas',
      desc: '',
      args: [],
    );
  }

  /// `Soups`
  String get soups {
    return Intl.message(
      'Soups',
      name: 'soups',
      desc: '',
      args: [],
    );
  }

  /// `Coctails`
  String get coctails {
    return Intl.message(
      'Coctails',
      name: 'coctails',
      desc: '',
      args: [],
    );
  }

  /// `Order comments`
  String get orderComments {
    return Intl.message(
      'Order comments',
      name: 'orderComments',
      desc: '',
      args: [],
    );
  }

  /// `Save order`
  String get saveOrder {
    return Intl.message(
      'Save order',
      name: 'saveOrder',
      desc: '',
      args: [],
    );
  }

  /// `Orders history`
  String get ordersHistory {
    return Intl.message(
      'Orders history',
      name: 'ordersHistory',
      desc: '',
      args: [],
    );
  }

  /// `No orders`
  String get noOrders {
    return Intl.message(
      'No orders',
      name: 'noOrders',
      desc: '',
      args: [],
    );
  }

  /// `No items`
  String get noDishes {
    return Intl.message(
      'No items',
      name: 'noDishes',
      desc: '',
      args: [],
    );
  }

  /// `Optionally`
  String get optionally {
    return Intl.message(
      'Optionally',
      name: 'optionally',
      desc: '',
      args: [],
    );
  }

  /// `To choose`
  String get toChoose {
    return Intl.message(
      'To choose',
      name: 'toChoose',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant email`
  String get restaurantEmail {
    return Intl.message(
      'Restaurant email',
      name: 'restaurantEmail',
      desc: '',
      args: [],
    );
  }

  /// `Save email`
  String get saveEmail {
    return Intl.message(
      'Save email',
      name: 'saveEmail',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<L> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'pl'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<L> load(Locale locale) => L.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
