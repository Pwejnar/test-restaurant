import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:test_restaurant/routes.dart';
import 'package:test_restaurant/ui/screens/home_screen/home_screen.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_colors.dart';
import 'package:test_restaurant/ui/shared/shared_variables.dart';
import 'package:test_restaurant/ui/shared/widgets/spinner_window.dart';

import 'generated/l10n.dart';

void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  registerServices(ConfigurationType.MOCK);
  _enablePlatformOverrideForDesktop();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GlobalKey<NavigatorState>? _key;

  final Widget Function(BuildContext, Widget) botToastBuilder = BotToastInit();

  @override
  void initState() {
    super.initState();
    _key = GlobalKey<NavigatorState>();

    SharedVariables.globalKey = _key;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: _key,
      title: "Restaurant",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: SharedColors.rose,
        secondaryHeaderColor: SharedColors.beige,
        brightness: Brightness.light,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          secondary: Colors.white,
        ),
      ),
      navigatorObservers: [BotToastNavigatorObserver()],
      localizationsDelegates: [
        L.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('pl', 'PL'),
      ],
      home: HomeScreen(),
      onGenerateRoute: generateRoutes,
      builder: (BuildContext context, Widget? page) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: botToastBuilder(context, SpinnerWindow(child: page)),
        );
      },
    );
  }
}
