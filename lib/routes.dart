import 'package:flutter/material.dart';
import 'package:test_restaurant/ui/screens/basket_screen/basket_screen.dart';
import 'package:test_restaurant/ui/screens/home_screen/home_screen.dart';
import 'package:test_restaurant/ui/screens/create_order_screen/create_order_screen.dart';
import 'package:test_restaurant/ui/screens/orders_history_screen/orders_history_screen.dart';

import 'package:test_restaurant/ui/screens/settings_screen/settings_screen.dart';

Route<dynamic> generateRoutes(RouteSettings settings) {
  if (settings.name == RouteNames.basketScreen) {
    return FadeRoute(
      RouteSettings(name: settings.name),
      page: BasketScreen(),
    );
  }

  if (settings.name == RouteNames.createOrderScreen) {
    return FadeRoute(
      RouteSettings(name: settings.name),
      page: CreateOrderScreen(),
    );
  }

  if (settings.name == RouteNames.previousOrdersScreen) {
    return FadeRoute(
      RouteSettings(name: settings.name),
      page: OrdersHistoryScreen(),
    );
  }

  if (settings.name == RouteNames.settingsScreen) {
    return FadeRoute(
      RouteSettings(name: settings.name),
      page: SettingsScreen(),
    );
  }

  return FadeRoute(
    RouteSettings(name: settings.name),
    page: HomeScreen(),
  );
}

class RouteNames {
  static const String homeScreen = "home";
  static const String createOrderScreen = "orderComments";
  static const String basketScreen = "basket";
  static const String previousOrdersScreen = "previousOrders";
  static const String settingsScreen = "settings";
}

class FadeRoute extends PageRouteBuilder {
  final Widget? page;
  final RouteSettings settings;

  FadeRoute(this.settings, {this.page})
      : super(
          settings: settings,
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) {
            return page!;
          },
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            return FadeTransition(
              opacity: animation,
              child: child,
            );
          },
        );
}
