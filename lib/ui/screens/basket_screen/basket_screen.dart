import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/routes.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_app_bar.dart';
import 'package:test_restaurant/ui/shared/widgets/default_scaffold.dart';
import 'package:test_restaurant/ui/shared/widgets/project/basket/basket_list_widget.dart';
import 'package:test_restaurant/ui/shared/widgets/project/header_text.dart';
import 'package:test_restaurant/ui/shared/widgets/rounded_button.dart';

class BasketScreen extends StatelessWidget {
  BasketScreen({Key? key}) : super(key: key);
  final _basketState = serviceLocator.get<BasketStateBase>();

  @override
  Widget build(BuildContext context) {
    return DefaultScaffold(
      appbar: CustomAppBar(title: _getTotalCost()),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SharedSizes.horizontalContentPadding,
          vertical: SharedSizes.verticalContentPadding,
        ),
        child: Column(
          children: [
            Expanded(child: BasketListWidget(items: _basketState.orderItems)),
            RoundedButton(
              title: L.of(context).next,
              onTap: () => _goNext(context),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getTotalCost() {
    return Observer(
      builder: (context) {
        double totalCost = 0;
        totalCost = _basketState.getTotalCost();
        String costString = totalCost.toStringAsFixed(2);

        return HeaderText(title: "$costString zł");
      },
    );
  }

  void _goNext(BuildContext context) {
    Navigator.of(context).pushNamed(RouteNames.createOrderScreen);
  }
}
