import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/order/order_model.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/core/services/base/order_service_base.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_methods.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_app_bar.dart';
import 'package:test_restaurant/ui/shared/widgets/default_scaffold.dart';
import 'package:test_restaurant/ui/shared/widgets/my_input.dart';
import 'package:test_restaurant/ui/shared/widgets/project/header_text.dart';
import 'package:test_restaurant/ui/shared/widgets/rounded_button.dart';
import 'package:uuid/uuid.dart';

class CreateOrderScreen extends StatefulWidget {
  const CreateOrderScreen({Key? key}) : super(key: key);

  @override
  State<CreateOrderScreen> createState() => _CreateOrderScreenState();
}

class _CreateOrderScreenState extends State<CreateOrderScreen> {
  final _basketState = serviceLocator.get<BasketStateBase>();
  final _orderService = serviceLocator.get<OrderServiceBase>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Key _commentsKey = Key("comments");
  String? _comments;

  @override
  Widget build(BuildContext context) {
    return DefaultScaffold(
      appbar: CustomAppBar(
        title: HeaderText(title: L.of(context).orderComments),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SharedSizes.horizontalContentPadding,
          vertical: SharedSizes.verticalContentPadding,
        ),
        child: Column(
          children: [
            SizedBox(height: SharedSizes.verticalSpacing),
            Align(
              alignment: Alignment.topCenter,
              child: Form(
                key: _formKey,
                child: MyInput(
                  key: _commentsKey,
                  borderColor: Colors.black.withOpacity(0.3),
                  contentPadding: EdgeInsets.all(SharedSizes.contentPadding),
                  label: L.of(context).orderComments,
                  hint: L.of(context).orderComments,
                  maxLines: 10,
                  validator: (String? s) => null,
                  onSaved: (String? s) {
                    _comments = s;
                  },
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RoundedButton(
                  title: L.of(context).saveOrder,
                  onTap: _saveOrder,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _saveOrder() async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (_formKey.currentState!.validate() != true) return;

    _formKey.currentState!.save();

    var newOrder = OrderModel(
      id: Uuid().v4(),
      items: _basketState.orderItems,
      comments: _comments,
    );

    await SharedMethods.fireFuture(_orderService.createOrder(newOrder));
    Navigator.of(context).popUntil((x) => x.isFirst);
    Future.delayed(Duration(milliseconds: 300), () => _basketState.clearAll());
  }
}
