import 'package:badges/badges.dart';
import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_restaurant/core/data/states/app_state.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/core/services/base/settings_service_Base.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/routes.dart';
import 'package:test_restaurant/ui/screens/home_screen/tabs/drinks_tab.dart';
import 'package:test_restaurant/ui/screens/home_screen/tabs/main_dishes_tab.dart';
import 'package:test_restaurant/ui/screens/home_screen/tabs/pizzas_tab.dart';
import 'package:test_restaurant/ui/screens/home_screen/tabs/soups_tab.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_colors.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_app_bar.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_future_builder.dart';
import 'package:test_restaurant/ui/shared/widgets/default_scaffold.dart';
import 'package:test_restaurant/ui/shared/widgets/project/header_text.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  final _appState = serviceLocator.get<AppStateBase>();
  final _basketState = serviceLocator.get<BasketStateBase>();
  final _settingsService = serviceLocator.get<SettingsServiceBase>();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 4,
      vsync: this,
      initialIndex: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultScaffold(
      appbar: CustomAppBar(
        title: _getTotalCost(),
        leading: Row(
          children: [
            IconButton(
              icon: Icon(Icons.history),
              onPressed: _showPreviousOrders,
            ),
          ],
        ),
        trailing: Row(
          children: [
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: _showSettings,
            ),
            IconButton(
              icon: Icon(Icons.delete_sweep),
              onPressed: _removeAllItems,
            ),
            _getBasketButton(),
          ],
        ),
      ),
      body: CustomFutureBuilder<String?>(
          showNoData: false,
          future: _settingsService.getRestaruantEmail,
          runOnce: true,
          builder: (restaurantEmail) {
            if (restaurantEmail != null) {
              _appState.setRestaurantEmail(restaurantEmail);
            }

            return Column(
              children: [
                TabBar(
                  isScrollable: true,
                  controller: _tabController,
                  labelStyle: TextStyle(
                    fontSize: SharedSizes.normalFontSize,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).secondaryHeaderColor,
                  ),
                  labelColor: Theme.of(context).primaryColor,
                  indicatorColor: Theme.of(context).primaryColor,
                  unselectedLabelColor: SharedColors.gray.withOpacity(0.5),
                  indicatorSize: TabBarIndicatorSize.label,
                  tabs: [
                    Tab(text: L.of(context).mainDishes),
                    Tab(text: L.of(context).pizzas),
                    Tab(text: L.of(context).soups),
                    Tab(text: L.of(context).coctails),
                  ],
                ),
                SizedBox(height: SharedSizes.verticalSpacing),
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      MainDishesTab(),
                      PizzasTab(),
                      SoupsTab(),
                      DrinksTab(),
                    ],
                  ),
                ),
              ],
            );
          }),
    );
  }

  Widget _getBasketButton() {
    return Observer(
      builder: (context) {
        int counts = _basketState.orderItems.length;
        var basketBtn = IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: _showBasket,
        );
        if (counts == 0) return basketBtn;

        return Padding(
          padding: EdgeInsets.only(right: SharedSizes.horizontalContentPadding),
          child: badges.Badge(
            badgeStyle: BadgeStyle(
              badgeColor: Theme.of(context).primaryColor,
            ),
            position: BadgePosition.topEnd(top: SharedSizes.badgeTopMargin),
            badgeContent: Text(
              counts.toString(),
              style: TextStyle(color: Colors.white),
            ),
            child: basketBtn,
          ),
        );
      },
    );
  }

  Widget _getTotalCost() {
    return Observer(
      builder: (context) {
        double totalCost = 0;
        totalCost = _basketState.getTotalCost();
        String costString = totalCost.toStringAsFixed(2);

        return HeaderText(title: "$costString zł");
      },
    );
  }

  _removeAllItems() {
    _basketState.clearAll();
  }

  void _showBasket() {
    Navigator.of(context).pushNamed(RouteNames.basketScreen);
  }

  void _showPreviousOrders() {
    Navigator.of(context).pushNamed(RouteNames.previousOrdersScreen);
  }

  void _showSettings() {
    Navigator.of(context).pushNamed(RouteNames.settingsScreen);
  }
}
