import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/core/services/base/manu_service_base.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_future_builder.dart';
import 'package:test_restaurant/ui/shared/widgets/project/menu/menu_list_widget.dart';

class DrinksTab extends StatefulWidget {
  const DrinksTab({Key? key}) : super(key: key);

  @override
  State<DrinksTab> createState() => _DrinksTabState();
}

class _DrinksTabState extends State<DrinksTab>
    with AutomaticKeepAliveClientMixin {
  var _menuService = serviceLocator.get<MenuServiceBase>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return CustomFutureBuilder<List<MenuItemModel>>(
      runOnce: true,
      future: _menuService.fetchDrinks,
      showBackground: false,
      builder: (items) {
        return MenuListWidget(items: items);
      },
    );
  }
}
