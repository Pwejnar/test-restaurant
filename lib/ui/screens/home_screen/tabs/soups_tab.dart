import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/core/services/base/manu_service_base.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_future_builder.dart';
import 'package:test_restaurant/ui/shared/widgets/project/menu/menu_list_widget.dart';

class SoupsTab extends StatefulWidget {
  const SoupsTab({Key? key}) : super(key: key);

  @override
  State<SoupsTab> createState() => _SoupsTabState();
}

class _SoupsTabState extends State<SoupsTab>
    with AutomaticKeepAliveClientMixin {
  var _menuService = serviceLocator.get<MenuServiceBase>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return CustomFutureBuilder<List<MenuItemModel>>(
      runOnce: true,
      future: _menuService.fetchSoups,
      showBackground: false,
      builder: (soups) {
        return MenuListWidget(items: soups);
      },
    );
  }
}
