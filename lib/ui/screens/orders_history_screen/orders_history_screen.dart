import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/order/order_history_model.dart';
import 'package:test_restaurant/core/services/base/order_service_base.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_app_bar.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_future_builder.dart';
import 'package:test_restaurant/ui/shared/widgets/default_scaffold.dart';
import 'package:test_restaurant/ui/shared/widgets/message_icon.dart';
import 'package:test_restaurant/ui/shared/widgets/project/header_text.dart';
import 'package:test_restaurant/ui/shared/widgets/project/order_tile_widget.dart';

class OrdersHistoryScreen extends StatelessWidget {
  OrdersHistoryScreen({Key? key}) : super(key: key);

  final _orderService = serviceLocator.get<OrderServiceBase>();

  @override
  Widget build(BuildContext context) {
    return DefaultScaffold(
      appbar: CustomAppBar(
        title: HeaderText(title: L.of(context).ordersHistory),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SharedSizes.horizontalContentPadding,
          vertical: SharedSizes.verticalContentPadding,
        ),
        child: CustomFutureBuilder<OrderHistoryModel?>(
          future: _orderService.fetchSavedOrders,
          showBackground: false,
          showNoData: false,
          builder: (order) {
            if (order?.orders?.isNotEmpty != true)
              return MessageIcon(title: L.of(context).noOrders);
            return ListView.builder(
              itemCount: order?.orders?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                var o = order?.orders?[index];
                return OrderTileWidget(order: o!);
              },
            );
          },
        ),
      ),
    );
  }
}
