import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/states/app_state.dart';
import 'package:test_restaurant/core/services/base/settings_service_Base.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_methods.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_app_bar.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_future_builder.dart';
import 'package:test_restaurant/ui/shared/widgets/default_scaffold.dart';
import 'package:test_restaurant/ui/shared/widgets/my_input.dart';
import 'package:test_restaurant/ui/shared/widgets/project/header_text.dart';
import 'package:test_restaurant/ui/shared/widgets/rounded_button.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _settingsService = serviceLocator.get<SettingsServiceBase>();
  final _appState = serviceLocator.get<AppStateBase>();

  Key _emailKey = Key("email");
  String? _email;

  @override
  Widget build(BuildContext context) {
    return DefaultScaffold(
      appbar: CustomAppBar(
        title: HeaderText(title: L.of(context).orderComments),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SharedSizes.horizontalContentPadding,
          vertical: SharedSizes.verticalContentPadding,
        ),
        child: Column(
          children: [
            SizedBox(height: SharedSizes.verticalSpacing),
            Align(
              alignment: Alignment.topCenter,
              child: Form(
                key: _formKey,
                child: CustomFutureBuilder<String?>(
                    showNoData: false,
                    future: _settingsService.getRestaruantEmail,
                    runOnce: true,
                    showBackground: false,
                    builder: (email) {
                      return MyInput(
                        key: _emailKey,
                        initialValue: email,
                        hint: L.of(context).restaurantEmail,
                        label: L.of(context).restaurantEmail,
                        borderColor: Colors.black.withOpacity(0.3),
                        contentPadding: EdgeInsets.all(
                          SharedSizes.contentPadding,
                        ),
                        validator: (String? s) => null,
                        onSaved: (String? s) {
                          _email = s;
                        },
                      );
                    }),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RoundedButton(
                  title: L.of(context).saveEmail,
                  onTap: _saveEmail,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _saveEmail() async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (_formKey.currentState!.validate() != true) return;

    _formKey.currentState!.save();

    if (_email != null) {
      _appState.setRestaurantEmail(_email!);
      await SharedMethods.fireFuture(
        _settingsService.setRestaurantEmail(_email!),
      );
    }

    Navigator.of(context).popUntil((x) => x.isFirst);
  }
}
