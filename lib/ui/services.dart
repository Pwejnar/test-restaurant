import 'package:get_it/get_it.dart';
import 'package:test_restaurant/core/data/states/app_state.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/core/services/base/email_service_base.dart';
import 'package:test_restaurant/core/services/base/manu_service_base.dart';
import 'package:test_restaurant/core/services/base/order_service_base.dart';
import 'package:test_restaurant/core/services/base/settings_service_Base.dart';
import 'package:test_restaurant/core/services/mock/email_service_mock.dart';
import 'package:test_restaurant/core/services/mock/menu_service_mock.dart';
import 'package:test_restaurant/core/services/mock/order_service_mock.dart';
import 'package:test_restaurant/core/services/mock/settings_service_mock.dart';

final GetIt serviceLocator = GetIt.instance;

enum ConfigurationType { MOCK, FIREBASE }

registerServices(ConfigurationType configuration) {
  serviceLocator.registerSingleton<AppStateBase>(AppState());
  serviceLocator.registerSingleton<BasketStateBase>(BasketState());

  serviceLocator.registerLazySingleton<MenuServiceBase>(
    () {
      return MenuServiceMock();
    },
  );
  serviceLocator.registerLazySingleton<OrderServiceBase>(
    () {
      return OrderServiceMock();
    },
  );

  serviceLocator.registerLazySingleton<EmailServiceBase>(
    () {
      return EmailServiceMock();
    },
  );

  serviceLocator.registerLazySingleton<SettingsServiceBase>(
    () {
      return SettingsServiceMock();
    },
  );
}
