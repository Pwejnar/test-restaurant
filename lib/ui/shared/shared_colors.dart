import 'package:flutter/material.dart';

class SharedColors {
  static const Color orange = Color.fromRGBO(234, 111, 100, 1);
  static const Color beige = Color.fromRGBO(255, 240, 229, 1);
  static const Color rose = Color.fromRGBO(227, 11, 94, 1);

  static const Color red = Color.fromRGBO(196, 34, 31, 1);
  static const Color grey = Color.fromRGBO(191, 191, 191, 1);
  static const Color blue = Color.fromRGBO(63, 75, 154, 1);

  static const Color lightGrey = Color.fromRGBO(223, 223, 223, 0.7);
  static const Color transparentLight = Color.fromRGBO(112, 112, 112, 0.2);
  static Color transparentDark = Colors.black.withOpacity(0.7);

  static const Color green = Color.fromRGBO(1, 128, 98, 1.0);

  static const Color gray = Color.fromRGBO(38, 38, 38, 1.0);
  static const Color grayText = Color.fromRGBO(206, 207, 219, 1.0);
  static const Color white = Color.fromRGBO(255, 255, 255, 1.0);

  static const Color blueTelegram = Color.fromRGBO(67, 136, 185, 1.0);
  static const Color grayChat = Color.fromRGBO(232, 230, 232, 1.0);
  static const Color blueChat = Color.fromRGBO(43, 82, 120, 1.0);
}
