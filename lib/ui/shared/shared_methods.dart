import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/order/order_model.dart';
import 'package:test_restaurant/core/data/states/app_state.dart';
import 'package:test_restaurant/core/services/log_handler.dart';
import 'package:test_restaurant/ui/services.dart';

class SharedMethods {
  static Color getFontColor(Color backgroundColor) {
    return backgroundColor.computeLuminance() < 0.5
        ? Colors.white
        : Colors.black;
  }

  static Future<T?> fireFuture<T>(Future<T> function) async {
    var appState = serviceLocator.get<AppStateBase>();
    T? data;

    try {
      appState.setLoading(true);
      data = await function;
    } catch (e, stacktrace) {
      appState.setLoading(false);
      print("----ERROR------:");
      LogHandler.showError(e, stacktrace);
    }

    appState.setLoading(false);
    return data;
  }

  static String getOrderDetails(OrderModel order) {
    String info = "";

    if (order.id != null) {
      info += "Order number:";
      info += "\n";
      info += order.id!;
      info += "\n----\n";
    }

    for (var i in order.items ?? []) {
      var name = i.menuItem?.name ?? "-";
      info += name;
      info += "\n";

      for (var so in i.selectedOptions) {
        var idOptionType = so.idOptionType;

        var x = i.menuItem?.optionTypes?.firstWhere(
          (e) => e.id == idOptionType,
        );

        var ingredients = x?.options
            ?.where((opt) => so.idSelections.contains(opt.id))
            .toList();

        for (var ingr in (ingredients ?? [])) {
          info += ingr.name ?? "-";
          info += "\n";
        }
      }
      info += "-----\n";
    }

    if (order.comments?.isNotEmpty == true) {
      info += '"${order.comments}"';
    }

    return info;
  }
}
