class SharedSizes {
  static double horizontalContentPadding = 20.0;
  static double verticalContentPadding = 20.0;
  static double verticalSpacing = 10.0;
  static double horizontalSpacing = 10.0;
  static double contentPadding = 10.0;
  static double badgeTopMargin = 2.0;
  static double borderRadius = 20.0;
  static double circleDecorationSize = 10.0;
  static double appBarHeight = 60.0;
  static double normalFontSize = 18.0;
  static double bigFontSize = 26.0;
}
