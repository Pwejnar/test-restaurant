import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/ui/shared/shared_variables.dart';

class SharedValidators {
  static BuildContext? get context => SharedVariables.globalKey?.currentContext;

  static String? validateName(String? value) {
    String? isLong = validateLong(value);
    String? isAlpha = _validateIfText(value);
    if (isLong != null) return isLong;
    if (isAlpha != null) return isAlpha;
    return null;
  }

  static String? _validateIfText(String? value) {
    if (isNumeric(value ?? "")) return L.of(context!).onlyLettersAllowed;
    return null;
  }

  static String? validateLong(String? value, {int long = 3}) {
    if ((value ?? "").length < long) {
      return L.of(context!).textTooShort;
    }

    return null;
  }
}
