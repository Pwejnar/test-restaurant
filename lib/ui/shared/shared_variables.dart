import 'package:flutter/material.dart';

class SharedVariables {
  static GlobalKey<NavigatorState>? globalKey;

  static String ordersSPKey = "allOrders";
  static String emailSPKey = "email";
}
