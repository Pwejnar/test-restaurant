import 'package:flutter/material.dart';

import 'spinner.dart';

class AnimatedSpinner extends StatelessWidget {
  AnimatedSpinner({
    required this.isLoading,
    required this.backgroundColor,
    required this.spinnerColor,
  });

  final bool? isLoading;
  final Color backgroundColor;
  final Color spinnerColor;

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !isLoading!,
      child: AnimatedOpacity(
        opacity: isLoading! ? 1.0 : 0.0,
        curve: Curves.fastOutSlowIn,
        duration: Duration(milliseconds: 700),
        child: Container(
          color: backgroundColor,
          child: isLoading!
              ? Spinner(
                  color: spinnerColor,
                )
              : null,
        ),
      ),
    );
  }
}
