import 'package:flutter/material.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';

import '../shared_methods.dart';

class CircleDecoration extends StatelessWidget {
  const CircleDecoration({
    Key? key,
    this.title,
    this.color,
    this.size,
  }) : super(key: key);

  final String? title;
  final Color? color;
  final double? size;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size ?? _getSize(),
      width: size ?? _getSize(),
      child: Center(
        child: Text(
          _getTitleLetter(),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: _getFontColor(context),
          ),
        ),
      ),
      decoration: BoxDecoration(
        color: _getColor(context),
        shape: BoxShape.circle,
      ),
    );
  }

  String _getTitleLetter() {
    if (title == null || title?.length == 0) return "";
    return title![0];
  }

  Color _getColor(context) {
    return color ?? Theme.of(context).primaryColor;
  }

  Color _getFontColor(context) {
    return SharedMethods.getFontColor(_getColor(context));
  }

  double _getSize() {
    return SharedSizes.circleDecorationSize;
  }
}
