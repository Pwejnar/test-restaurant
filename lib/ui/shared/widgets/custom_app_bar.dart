import 'package:flutter/material.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';

import '../shared_methods.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Color? color;
  final Widget? title;
  final Widget? leading;
  final Widget? trailing;

  const CustomAppBar({
    Key? key,
    this.color,
    this.title,
    this.leading,
    this.trailing,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(SharedSizes.appBarHeight);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  late Color fontColor;

  @override
  Widget build(BuildContext context) {
    Color appBarColor = widget.color ?? Theme.of(context).secondaryHeaderColor;
    fontColor = SharedMethods.getFontColor(appBarColor);

    return AppBar(
      automaticallyImplyLeading: true,
      backgroundColor: appBarColor,
      centerTitle: true,
      foregroundColor: Colors.white,
      iconTheme: IconThemeData(color: fontColor),
      elevation: 0.0,
      leading: widget.leading,
      title: widget.title,
      actions: [
        if (widget.trailing != null) widget.trailing!,
      ],
    );
  }
}
