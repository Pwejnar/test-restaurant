import 'package:flutter/material.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/type_defs.dart';

class CustomCheckBox extends StatelessWidget {
  final String title;
  final OnSelected<bool?> onChanged;
  final bool value;

  const CustomCheckBox({
    Key? key,
    required this.title,
    required this.onChanged,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChanged(!value),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(
            value: value,
            activeColor: Theme.of(context).secondaryHeaderColor,
            checkColor: Theme.of(context).primaryColor,
            onChanged: (v) => onChanged(v),
          ),
          SizedBox(width: SharedSizes.horizontalSpacing / 2),
          Text(title),
        ],
      ),
    );
  }
}
