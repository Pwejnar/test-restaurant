import 'package:async/async.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:flutter/material.dart';

import 'message_icon.dart';
import 'spinner_wrapper.dart';

typedef BuilderData<T> = Widget Function(T? data);
typedef ReturnData<T> = void Function(T data);
typedef FutureFunction<T> = Future<T> Function();

enum LoadingState {
  loading,
  error,
  noData,
  gotData,
}

class CustomFutureBuilder<T> extends StatefulWidget {
  CustomFutureBuilder({
    Key? key,
    this.runOnce = false,
    this.showNoData = true,
    required this.future,
    required this.builder,
    this.onDataLoaded,
    this.showBackground = true,
    this.loadingWidget,
    this.noDataWidget,
    this.problemWidget,
    child,
  }) : super(key: key);

  final bool runOnce;
  final bool showNoData;
  final FutureFunction<T> future;
  final ReturnData<T>? onDataLoaded;
  final BuilderData<T> builder;
  final bool showBackground;
  final Widget? loadingWidget;
  final Widget? noDataWidget;
  final Widget? problemWidget;

  @override
  _CustomFutureBuilderState<T> createState() => _CustomFutureBuilderState<T>();
}

class _CustomFutureBuilderState<T> extends State<CustomFutureBuilder<T>> {
  AsyncMemoizer<T>? _memoizer;
  T? _data;

  @override
  void initState() {
    super.initState();
    if (widget.runOnce) {
      _memoizer = AsyncMemoizer<T>();
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<T>(
      key: widget.key,
      future: getFutureWrapper(),
      builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
        LoadingState state = _getLoadingState(snapshot);
        T? data = snapshot.data;

        if (state == LoadingState.gotData) {
          bool isFirstTime = _data == null;
          _data = data;

          if (widget.onDataLoaded != null && isFirstTime) {
            widget.onDataLoaded!(data!);
          }
        }

        if (state == LoadingState.error) {
          print("----FUTURE BUILDER ERROR------:");
          print(snapshot.error);
          return widget.problemWidget ??
              Material(
                child: Center(
                  child: MessageIcon(
                    title: L.of(context).problemOccured,
                  ),
                ),
              );
        }

        if (state == LoadingState.noData && widget.showNoData)
          return widget.noDataWidget ??
              MessageIcon(title: L.of(context).noData);

        bool isLoading = state == LoadingState.loading;

        if (isLoading && widget.loadingWidget != null) {
          return widget.loadingWidget!;
        }

        return SpinnerWrapper(
          isLoading: isLoading,
          showBackground: widget.showBackground,
          child: Builder(
            builder: (BuildContext context) {
              if (isLoading && widget.showBackground == false)
                return Container();

              T? dataWrapper = data;
              Widget w = widget.builder(dataWrapper);
              return w;
            },
          ),
        );
      },
    );
  }

  LoadingState _getLoadingState(AsyncSnapshot snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return LoadingState.loading;
    }
    if (snapshot.hasError) {
      return LoadingState.error;
    }
    if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
      return LoadingState.gotData;
    }
    return LoadingState.noData;
  }

  Future<T> getFutureWrapper() {
    if (widget.runOnce) {
      return _memoizer!.runOnce(() {
        return getFuture();
      });
    }

    return getFuture();
  }

  Future<T> getFuture() {
    return widget.future();
  }
}
