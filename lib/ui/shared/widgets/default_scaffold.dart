import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_restaurant/ui/shared/shared_colors.dart';

class DefaultScaffold extends StatefulWidget {
  DefaultScaffold({
    Key? key,
    required this.body,
    this.backgroundColor,
    this.appbar,
    this.title,
  }) : super(key: key);

  final Widget body;
  final Color? backgroundColor;
  final Widget? appbar;
  final String? title;

  @override
  DefaultScaffoldState createState() {
    return DefaultScaffoldState();
  }
}

class DefaultScaffoldState extends State<DefaultScaffold> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: SafeArea(
        child: Scaffold(
          primary: true,
          backgroundColor: SharedColors.white,
          resizeToAvoidBottomInset: true,
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          appBar: widget.appbar as PreferredSizeWidget?,
          body: widget.body,
        ),
      ),
    );
  }
}
