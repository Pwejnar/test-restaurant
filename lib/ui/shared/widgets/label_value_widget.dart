import 'package:flutter/widgets.dart';

class LabelValueWidget extends StatelessWidget {
  final String? label;
  final String? value;

  const LabelValueWidget({Key? key, this.label, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (label != null) Text(label!),
        if (value != null)
          Text(
            value!,
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
      ],
    );
  }
}
