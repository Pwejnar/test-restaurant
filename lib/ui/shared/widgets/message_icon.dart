import 'package:flutter/material.dart';
import 'package:test_restaurant/ui/shared/shared_colors.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';

class MessageIcon extends StatefulWidget {
  const MessageIcon({
    Key? key,
    this.title,
    this.icon,
    this.backgroundColor,
  }) : super(key: key);

  final String? title;
  final IconData? icon;
  final Color? backgroundColor;

  @override
  _MessageIconState createState() => _MessageIconState();
}

class _MessageIconState extends State<MessageIcon> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: widget.backgroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Icon(
              widget.icon ?? Icons.info_outline,
              size: 70.0,
              color: Theme.of(context).primaryColor.withOpacity(0.3),
            ),
            SizedBox(height: SharedSizes.horizontalSpacing),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SharedSizes.horizontalContentPadding,
              ),
              child: Container(
                width: 300,
                child: Text(
                  widget.title ?? "",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: SharedSizes.bigFontSize,
                    color: SharedColors.transparentLight.withOpacity(0.5),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
