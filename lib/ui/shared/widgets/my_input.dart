import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';

import '../shared_colors.dart';
import '../type_defs.dart';

enum BorderType { underline, outline }

class MyInput extends StatefulWidget {
  final IconData? icon;
  final String? label;
  final Color? labelColor;
  final ValidationType validator;
  final OnSelected<String>? onSaved;
  final TextEditingController? textController;
  final EdgeInsetsGeometry? contentPadding;
  final bool autoFocus;
  final int? maxLength;
  final String? hint;
  final String? suffix;
  final Color? underlineColor;
  final bool? autoValidate;
  final bool? enabled;
  final TextInputType? inputType;
  final bool obscureText;
  final bool? textCapitalization;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final VoidCallback? onSubmitted;
  final OnSelected<String>? onChanged;
  final Color? textColor;
  final Color? iconColor;
  final TextAlign? textAlign;
  final double? fontSize;
  final String? initialValue;
  final List<TextInputFormatter>? inputFormatters;
  final bool? isObligatory;
  final bool? hideLabelWhenSelected;
  final VoidCallback? onClear;
  final bool addBottomMargin;
  final BorderType? borderType;
  final Color? borderColor;
  final int? maxLines;
  final double? letterSpacing;
  final Widget? prefix;

  MyInput({
    required Key? key,
    this.icon,
    this.label,
    this.labelColor,
    required this.validator,
    this.onSaved,
    this.obscureText = false,
    this.autoFocus = false,
    this.enabled,
    this.inputType,
    this.textController,
    this.maxLength,
    this.hint,
    this.suffix,
    this.underlineColor,
    this.autoValidate,
    this.textCapitalization,
    this.focusNode,
    this.nextFocusNode,
    this.onSubmitted,
    this.onChanged,
    this.textColor,
    this.textAlign,
    this.fontSize,
    this.initialValue,
    this.iconColor,
    this.inputFormatters,
    this.isObligatory,
    this.hideLabelWhenSelected,
    this.onClear,
    this.addBottomMargin = true,
    this.borderType,
    this.borderColor,
    this.maxLines,
    this.letterSpacing,
    this.prefix,
    this.contentPadding,
  }) : super(key: key);

  @override
  MyInputState createState() {
    return MyInputState();
  }
}

class MyInputState extends State<MyInput> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(
              child: TextFormField(
                key: widget.key,
                onChanged: (s) {
                  if (widget.onChanged != null) {
                    widget.onChanged!(s);
                  }
                },
                enabled: widget.enabled ?? true,
                textAlign: widget.textAlign ?? TextAlign.start,
                textCapitalization: widget.textCapitalization == false
                    ? TextCapitalization.none
                    : TextCapitalization.sentences,
                controller: widget.textController,
                initialValue: widget.initialValue,
                obscureText: widget.obscureText,
                validator: (s) {
                  if (widget.isObligatory == false && (s == null || s == "")) {
                    return null;
                  }
                  return widget.validator(s);
                },
                inputFormatters: widget.inputFormatters != null
                    ? widget.inputFormatters
                    : widget.maxLength != null
                        ? [LengthLimitingTextInputFormatter(widget.maxLength)]
                        : null,
                maxLength: widget.maxLength,
                keyboardType: widget.inputType ?? TextInputType.text,
                maxLines: widget.inputType == TextInputType.multiline
                    ? null
                    : (widget.maxLines ?? 1),
                autofocus: widget.autoFocus,
                onFieldSubmitted: onSubmitted,
                onSaved: (dynamic value) {
                  if (value == null) return;
                  widget.onSaved!(value.toString());
                },
                style: TextStyle(
                  fontSize: widget.fontSize ?? SharedSizes.normalFontSize,
                  letterSpacing: widget.letterSpacing,
                  fontWeight: FontWeight.w500,
                  color: widget.textColor ?? Colors.black,
                ),
                decoration: InputDecoration(
                  prefix: widget.prefix,
                  hintText: widget.hint ?? "",
                  suffixIcon: widget.icon != null
                      ? Icon(
                          widget.icon,
                          color: widget.iconColor ?? widget.textColor,
                        )
                      : null,
                  hintStyle: TextStyle(
                    color: Colors.black.withOpacity(0.15),
                    fontWeight: FontWeight.normal,
                  ),
                  suffixText: widget.suffix ?? null,
                  label: Text(_getLabel() ?? ""),
                  alignLabelWithHint: true,
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: widget.labelColor ?? Colors.grey,
                  ),
                  errorMaxLines: 1,
                  errorStyle: TextStyle(fontSize: SharedSizes.normalFontSize),
                  contentPadding: widget.contentPadding ??
                      EdgeInsets.only(left: 0.0, bottom: 0),
                  suffixStyle: TextStyle(
                    fontSize: SharedSizes.normalFontSize,
                    color: Colors.black,
                  ),
                  enabledBorder: _getBorder(),
                  disabledBorder: _getBorder(),
                  focusedBorder: _getFocusedBorder(),
                  border: _getBorder(),
                ),
              ),
            ),
          ],
        ),
        if (widget.addBottomMargin == true) SizedBox(height: 15.0),
      ],
    );
  }

  InputBorder _getBorder() {
    if (widget.borderType == BorderType.underline) {
      return UnderlineInputBorder(
        borderSide: BorderSide(
          color: widget.borderColor ?? Theme.of(context).primaryColor,
        ),
      );
    }
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: widget.borderColor ?? SharedColors.lightGrey,
      ),
    );
  }

  InputBorder _getFocusedBorder() {
    if (widget.borderType == BorderType.underline) {
      return UnderlineInputBorder(
        borderSide: BorderSide(
          color: widget.borderColor ?? Theme.of(context).primaryColor,
        ),
      );
    }
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: widget.borderColor ?? Theme.of(context).primaryColor,
      ),
    );
  }

  String? _getLabel() {
    if (widget.label != null) {
      if (widget.hideLabelWhenSelected == true && widget.label != null) {
        return null;
      }
      if (widget.isObligatory != null) {
        return widget.isObligatory! ? widget.label! + " *" : widget.label;
      }
      if (widget.enabled == true) {
        return widget.label! + " *";
      }
      return widget.label;
    }
    return "";
  }

  void onSubmitted(_) {
    if (widget.nextFocusNode != null) {
      FocusScope.of(context).requestFocus(widget.nextFocusNode);
    }
    if (widget.onSubmitted != null) {
      widget.onSubmitted!();
    }
  }
}
