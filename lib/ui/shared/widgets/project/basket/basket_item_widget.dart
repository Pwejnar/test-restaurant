import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/order/order_item_model.dart';
import 'package:test_restaurant/ui/shared/widgets/project/menu/menu_item_widget.dart';

class BasketItemWidget extends StatefulWidget {
  final OrderItemModel item;
  final Widget? child;

  const BasketItemWidget({
    Key? key,
    required this.item,
    this.child,
  }) : super(key: key);

  @override
  State<BasketItemWidget> createState() => _BasketItemWidgetState();
}

class _BasketItemWidgetState extends State<BasketItemWidget> {
  @override
  Widget build(BuildContext context) {
    return MenuItemWidget(
      item: widget.item.menuItem!,
      child: widget.child,
    );
  }
}
