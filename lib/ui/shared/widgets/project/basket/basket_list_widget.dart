import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/order/order_item_model.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/message_icon.dart';
import 'package:test_restaurant/ui/shared/widgets/project/basket/basket_item_widget.dart';
import 'package:test_restaurant/ui/shared/widgets/project/basket/basket_option_type_widget.dart';

class BasketListWidget extends StatefulWidget {
  final List<OrderItemModel>? items;

  BasketListWidget({
    Key? key,
    this.items = const [],
  }) : super(key: key);

  @override
  State<BasketListWidget> createState() => _BasketListWidgetState();
}

class _BasketListWidgetState extends State<BasketListWidget> {
  final _basketState = serviceLocator.get<BasketStateBase>();

  @override
  Widget build(BuildContext context) {
    if (widget.items == null || widget.items?.isNotEmpty != true) {
      return MessageIcon(title: L.of(context).noDishes);
    }

    return ListView.builder(
      padding: EdgeInsets.symmetric(
        horizontal: SharedSizes.horizontalContentPadding,
      ),
      itemCount: widget.items?.length ?? 0,
      itemBuilder: (BuildContext context, int index) {
        OrderItemModel item = widget.items![index];
        var allOptions = item.menuItem?.optionTypes ?? [];

        return BasketItemWidget(
          item: item,
          child: Column(
            children: allOptions.map(
              (ot) {
                return BasketSelectOptionWidget(
                  optionType: ot,
                  idOrdeItem: item.id!,
                  onChangedState: (idOption) {
                    String idOrderItem = item.id!;
                    String idOptionType = ot.id!;
                    _changeState(idOrderItem, idOptionType, idOption);
                  },
                );
              },
            ).toList(),
          ),
        );
      },
    );
  }

  void _changeState(String idOrderItem, String idOptionType, String idOption) {
    _basketState.changeSelectionState(idOrderItem, idOptionType, idOption);
  }
}
