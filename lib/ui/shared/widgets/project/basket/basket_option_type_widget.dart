import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_type_model.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/type_defs.dart';
import 'package:test_restaurant/ui/shared/widgets/custom_checkbox.dart';

class BasketSelectOptionWidget extends StatefulWidget {
  final String idOrdeItem;
  final MenuOptionTypeModel optionType;
  final OnItemSelected<String> onChangedState;

  const BasketSelectOptionWidget({
    Key? key,
    required this.idOrdeItem,
    required this.optionType,
    required this.onChangedState,
  }) : super(key: key);

  @override
  State<BasketSelectOptionWidget> createState() =>
      _BasketSelectOptionWidgetState();
}

class _BasketSelectOptionWidgetState extends State<BasketSelectOptionWidget> {
  final _basketState = serviceLocator.get<BasketStateBase>();

  @override
  Widget build(BuildContext context) {
    var options = widget.optionType.options;
    if (options?.isNotEmpty != true) return Container();
    if (options?.length == 1) return Text(options!.first.name ?? "-");

    bool isMultiSelect = widget.optionType.allowCheckMany == true;

    if (isMultiSelect) {
      return _getMultiSelectOptions();
    } else {
      return _getSingleSelect();
    }
  }

  Widget _getSingleSelect() {
    var values = widget.optionType.options ?? [];

    return Column(
      children: values.map(
        (opt) {
          return Container(
            padding: EdgeInsets.symmetric(
              vertical: SharedSizes.verticalSpacing / 2,
              horizontal: SharedSizes.horizontalSpacing,
            ),
            child: InkWell(
              onTap: () => widget.onChangedState(opt.id!),
              child: Observer(
                builder: (context) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      if (_isEnabled(opt.id!))
                        Icon(
                          Icons.radio_button_checked,
                          color: Theme.of(context).primaryColor,
                        )
                      else
                        Icon(
                          Icons.radio_button_unchecked,
                        ),
                      SizedBox(width: SharedSizes.horizontalSpacing),
                      Builder(
                        builder: (context) {
                          bool isPaidAddition = (opt.price ?? 0) != 0;
                          return Text(
                            isPaidAddition
                                ? "${opt.name ?? "-"} (+${opt.price}zł)"
                                : "${opt.name ?? "-"}",
                            style: TextStyle(),
                          );
                        },
                      ),
                    ],
                  );
                },
              ),
            ),
          );
        },
      ).toList(),
    );
  }

  Widget _getMultiSelectOptions() {
    var values = widget.optionType.options ?? [];

    return Column(
      children: values.map(
        (opt) {
          bool isPaidAddition = (opt.price ?? 0) != 0;

          return Observer(builder: (context) {
            return CustomCheckBox(
              title: isPaidAddition
                  ? "${opt.name ?? "-"} (+${opt.price}zł)"
                  : "${opt.name ?? "-"}",
              value: _isEnabled(opt.id!),
              onChanged: (_) => widget.onChangedState(opt.id!),
            );
          });
        },
      ).toList(),
    );
  }

  bool _isEnabled(String idOption) {
    return _basketState.isSelected(
      widget.idOrdeItem,
      widget.optionType.id!,
      idOption,
    );
  }
}
