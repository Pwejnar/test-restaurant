import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  final String? title;

  const HeaderText({
    Key? key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title ?? "-",
      style: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black,
      ),
    );
  }
}
