import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';

class MenuItemAddRemoveWidget extends StatelessWidget {
  final MenuItemModel item;
  final String? title;
  final VoidCallback? onAdd;
  final VoidCallback? onRemove;

  MenuItemAddRemoveWidget({
    Key? key,
    required this.item,
    this.onAdd,
    this.onRemove,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: SharedSizes.horizontalSpacing),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            onPressed: onRemove,
            icon: Icon(Icons.remove),
          ),
          if (title != null)
            Expanded(
              child: Center(
                child: Text(
                  title!,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
          IconButton(
            onPressed: onAdd,
            icon: Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}
