import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/label_value_widget.dart';

class MenuItemWidget extends StatelessWidget {
  final MenuItemModel item;
  final Widget? child;

  const MenuItemWidget({
    Key? key,
    required this.item,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var price = item.price ?? 0;
    var priceString = "${price.toStringAsFixed(2)} zł";

    return Container(
      margin: EdgeInsets.only(bottom: SharedSizes.verticalContentPadding),
      padding: EdgeInsets.all(SharedSizes.verticalContentPadding / 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SharedSizes.borderRadius),
        color: Colors.white,
        border: Border.all(
          color: Colors.black.withOpacity(0.3),
        ),
      ),
      child: Column(
        children: [
          LabelValueWidget(value: item.name),
          LabelValueWidget(label: priceString),
          if (child != null) child!,
        ],
      ),
    );
  }
}
