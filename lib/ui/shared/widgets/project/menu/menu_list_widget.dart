import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_item_model.dart';
import 'package:test_restaurant/core/data/states/basket_state.dart';
import 'package:test_restaurant/generated/l10n.dart';
import 'package:test_restaurant/ui/services.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/message_icon.dart';
import 'package:test_restaurant/ui/shared/widgets/project/menu/menu_item_add_remove_widget.dart';
import 'package:test_restaurant/ui/shared/widgets/project/menu/menu_item_widget.dart';

class MenuListWidget extends StatelessWidget {
  final List<MenuItemModel>? items;
  final _basketState = serviceLocator.get<BasketStateBase>();

  MenuListWidget({Key? key, this.items = const []}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (items == null || items?.isNotEmpty != true) {
      return MessageIcon(title: L.of(context).noDishes);
    }

    return ListView.builder(
      padding: EdgeInsets.symmetric(
        horizontal: SharedSizes.horizontalContentPadding / 2,
        vertical: SharedSizes.verticalContentPadding / 2,
      ),
      itemCount: items?.length ?? 0,
      itemBuilder: (BuildContext context, int index) {
        MenuItemModel item = items![index];
        return MenuItemWidget(item: item, child: _getManageButtons(item));
      },
    );
  }

  Widget _getManageButtons(MenuItemModel item) {
    return Observer(
      builder: (context) {
        int itemBasketCount = 0;

        String? id = item.id;
        if (id != null) {
          itemBasketCount = _basketState.getBasketItemsCount(id);
        }

        return MenuItemAddRemoveWidget(
          item: item,
          title: itemBasketCount.toString(),
          onAdd: () => _addToBusket(item),
          onRemove: itemBasketCount > 0 ? () => _removeFromBasket(item) : null,
        );
      },
    );
  }

  void _addToBusket(MenuItemModel item) {
    _basketState.addItemToBasket(item);
  }

  void _removeFromBasket(MenuItemModel item) {
    String? idItem = item.id;
    if (idItem == null) return;
    _basketState.removeItemFromBasket(idItem);
  }
}
