import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_type_model.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/circle_decorations.dart';

import '../../../../../generated/l10n.dart';

class MenuOptionTypeWidget extends StatelessWidget {
  final MenuOptionTypeModel option;

  const MenuOptionTypeWidget({
    Key? key,
    required this.option,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var title = "";
    var options = option.options;

    if (options?.isNotEmpty != true) return Container();
    if (options!.length == 1)
      return Align(
        alignment: Alignment.centerLeft,
        child: Text(options.first.name ?? "-"),
      );

    if (option.allowCheckMany == true) {
      title = "${L.of(context).optionally}:";
    } else {
      title = "${L.of(context).toChoose}:";
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: SharedSizes.verticalSpacing),
        Text(
          title,
          style: TextStyle(fontWeight: FontWeight.w700),
        ),
        SizedBox(height: SharedSizes.verticalSpacing),
        ...options.map((e) {
          String name = e.name ?? "-";
          return Padding(
            padding: EdgeInsets.only(bottom: SharedSizes.horizontalSpacing / 2),
            child: Row(
              children: [
                CircleDecoration(),
                SizedBox(width: SharedSizes.horizontalSpacing / 2),
                Expanded(child: Text(name)),
              ],
            ),
          );
        }).toList(),
      ],
    );
  }
}
