import 'package:flutter/material.dart';
import 'package:test_restaurant/core/data/models/menu_item/menu_option_model.dart';
import 'package:test_restaurant/core/data/models/order/order_model.dart';
import 'package:test_restaurant/core/data/models/order/order_item_model.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';
import 'package:test_restaurant/ui/shared/widgets/project/basket/basket_item_widget.dart';

class OrderTileWidget extends StatelessWidget {
  final OrderModel order;

  const OrderTileWidget({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(SharedSizes.verticalContentPadding / 2),
      margin: EdgeInsets.only(bottom: SharedSizes.verticalContentPadding),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SharedSizes.borderRadius),
        color: Colors.white,
        border: Border.all(
          color: Colors.black.withOpacity(0.3),
        ),
      ),
      child: Builder(
        builder: (context) {
          var fullPrice = order.gotTotalPrice();

          return Column(
            children: [
              Column(
                children: order.items!.map((item) {
                  return BasketItemWidget(
                    item: item,
                    child: _getOptions(item),
                  );
                }).toList(),
              ),
              if (order.comments?.isNotEmpty == true)
                Padding(
                  padding: EdgeInsets.only(
                    bottom: SharedSizes.verticalSpacing / 2,
                  ),
                  child: Text('"${order.comments!}"'),
                ),
              Text("$fullPrice zł"),
            ],
          );
        },
      ),
    );
  }

  Widget _getOptions(OrderItemModel orderItem) {
    List<MenuOptionModel> selectedOptions = [];

    for (var o in orderItem.selectedOptions) {
      String idOptiontype = o.idOptionType;
      var ot = orderItem.menuItem!.optionTypes!.firstWhere(
        (e) => e.id == idOptiontype,
      );

      var options = ot.options!.where((f) => o.idSelections.contains(f.id));
      selectedOptions.addAll(options);
    }

    return Column(
      children: selectedOptions.map((e) {
        return Text(e.name ?? "-");
      }).toList(),
    );
  }
}
