import 'package:test_restaurant/ui/shared/shared_methods.dart';
import 'package:flutter/material.dart';
import 'package:test_restaurant/ui/shared/shared_sizes.dart';

enum IconPlacementSide {
  topLeft,
  topRight,
  bottomRight,
  bottomLeft,
}

class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key? key,
    this.title,
    this.icon,
    this.iconPlacement,
    this.iconColor,
    this.width,
    this.height,
    this.color,
    this.elevation,
    this.onTap,
    this.hideBackground,
    this.borderColor,
    this.onLongPress,
    this.enabled = true,
    this.isExpanded = true,
  }) : super(key: key);

  final String? title;
  final IconData? icon;
  final IconPlacementSide? iconPlacement;
  final Color? iconColor;
  final double? width;
  final double? height;
  final Color? color;
  final double? elevation;
  final VoidCallback? onTap;
  final VoidCallback? onLongPress;
  final bool? hideBackground;
  final Color? borderColor;
  final bool enabled;
  final bool isExpanded;

  @override
  Widget build(BuildContext context) {
    var bgColor = hideBackground == true
        ? Colors.transparent
        : (color ?? Theme.of(context).primaryColor);
    var fontColor = bgColor == Colors.transparent
        ? SharedMethods.getFontColor(Colors.white)
        : SharedMethods.getFontColor(bgColor);

    return Material(
      color: enabled ? bgColor : bgColor.withOpacity(0.4),
      borderRadius: BorderRadius.circular(SharedSizes.borderRadius),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        onTap: enabled != true ? null : onTap,
        onLongPress: onLongPress,
        child: Container(
          width: width,
          height: height ?? 50,
          padding: EdgeInsets.symmetric(
            horizontal: SharedSizes.horizontalContentPadding,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(SharedSizes.borderRadius),
            border:
                borderColor != null ? Border.all(color: borderColor!) : null,
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SharedSizes.horizontalSpacing / 2,
            ),
            child: Row(
              children: [
                if (isExpanded == true)
                  Expanded(child: _getText(fontColor))
                else
                  _getText(fontColor)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _getText(Color fontColor) {
    return Text(
      title ?? "-",
      textAlign: TextAlign.center,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: SharedSizes.normalFontSize,
        fontFamily: "Neometric",
        color: fontColor,
      ),
    );
  }
}
