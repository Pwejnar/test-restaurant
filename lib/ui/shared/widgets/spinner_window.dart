import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:test_restaurant/core/data/states/app_state.dart';
import 'package:test_restaurant/ui/services.dart';

import 'animated_spinner.dart';

class SpinnerWindow extends StatelessWidget {
  final Widget? child;

  SpinnerWindow({
    Key? key,
    this.child,
  }) : super(key: key);

  final _appState = serviceLocator.get<AppStateBase>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        if (child != null) child!,
        Observer(
          builder: (context) {
            return AnimatedSpinner(
              isLoading: _appState.isLoading,
              backgroundColor: Colors.black.withOpacity(0.3),
              spinnerColor: Colors.white,
            );
          },
        ),
      ],
    );
  }
}
