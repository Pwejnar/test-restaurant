import 'package:test_restaurant/ui/shared/shared_methods.dart';
import 'package:flutter/material.dart';

import 'animated_spinner.dart';

class SpinnerWrapper extends StatelessWidget {
  final Widget child;
  final bool? isLoading;
  final bool showBackground;
  final bool? isOldData;

  SpinnerWrapper({
    this.isLoading,
    required this.child,
    this.showBackground = true,
    this.isOldData,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        if (isLoading == false || (isLoading == true && showBackground)) child,
        if (isOldData != true)
          AnimatedSpinner(
            isLoading: isLoading,
            backgroundColor: _getBackgroundColor(context),
            spinnerColor: _getSpinnerColor(context),
          ),
      ],
    );
  }

  Color _getBackgroundColor(context) {
    return showBackground ? Colors.white.withOpacity(0.3) : Colors.white;
  }

  Color _getSpinnerColor(BuildContext context) {
    return SharedMethods.getFontColor(
      _getBackgroundColor(context),
    );
  }
}
